﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Checker.DataContracts;

namespace Checker.Client.Check
{
    public class CheckCompletedEventArgs : EventArgs
    {
        public long CmdId { get; set; }

        public CheckStatusEnum CheckResult { get; set; }

        public string SystemInfo { get; set; }

        public int? FailCode { get; set; }

        public string FailInfo { get; set; }
    }
}
