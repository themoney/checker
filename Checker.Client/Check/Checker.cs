﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using AxMSTSCLib;
using System.IO;
using System.Threading;
using Checker.Client.Config;
using Checker.DataContracts;
using System.Windows.Forms;

namespace Checker.Client.Check
{
    public class Checker : IChecker
    {
        private System.Timers.Timer timer { get; set; }
        private AxMsRdpClient7 client { get; set; }
        private CheckerStatusEnum status { get; set; }
        private CheckStatusEnum result { get; set; }
        private string id { get; set; }
        private bool sysInfoRequired { get; set; }
        private long commandId { get; set; }
        private StringBuilder failInfo { get; set; }
        private int? failCode { get; set; }
        private bool emptyPass { get; set; }

        public CheckerStatusEnum Status
        {
            get
            {
                return status;
            }
        }

        public Checker(AxMsRdpClient7 client)
        {
            this.client = client;
            this.client.OnConnected += new EventHandler(client_OnConnected);
            this.client.OnDisconnected += new IMsTscAxEvents_OnDisconnectedEventHandler(client_OnDisconnected);
            this.client.OnLoginComplete += new EventHandler(client_OnLoginComplete);
            this.client.OnLogonError += new IMsTscAxEvents_OnLogonErrorEventHandler(client_OnLogonError);
            this.client.OnFatalError += new IMsTscAxEvents_OnFatalErrorEventHandler(client_OnFatalError);
      
            status = CheckerStatusEnum.free;
            result = CheckStatusEnum.NotConnected;
            failInfo = new StringBuilder();

            timer = new System.Timers.Timer(ConfigHelper.CheckingTimeout * 1000);
            timer.Elapsed += new System.Timers.ElapsedEventHandler(timer_Elapsed);
        }

        private void timer_Elapsed(object sender, System.Timers.ElapsedEventArgs e)
        {
            failCode = 0;
            failInfo.AppendLine("Checking terminated by timer.");
            RaiseFormActionRequired(FormActionEventArgs.FormActionType.CaptureScreen);
        }        

        private void client_OnFatalError(object sender, IMsTscAxEvents_OnFatalErrorEvent e)
        {
            failCode = e.errorCode;
            failInfo.AppendFormat("Fatal error: {0}", e.errorCode);
            client.Disconnect();
        }

        private void client_OnLogonError(object sender, IMsTscAxEvents_OnLogonErrorEvent e)
        {
            failCode = e.lError;
            failInfo.Append("Logon error: ");

            switch (e.lError)
            {
                case -5:
                    failInfo.AppendLine("Winlogon is displaying the Session Contention dialog box.");
                    break;
                case -2:
                    failInfo.AppendLine("Winlogon is continuing with the logon process.");
                    break;
                case -3:
                    failInfo.AppendLine("Winlogon is ending silently.");
                    break;
                case -6:
                    failInfo.AppendLine("Winlogon is displaying the No Permissions dialog box.");
                    break;
                case -7:
                    failInfo.AppendLine("Winlogon is displaying the Disconnect Refused dialog box.");
                    break;
                case -4:
                    failInfo.AppendLine("Winlogon is displaying the Reconnect dialog box.");
                    break;
                case -1:
                    failInfo.AppendLine("The user was denied access.");
                    break;
                case 0:
                    failInfo.AppendLine("The logon failed because the logon credentials are not valid.");
                    break;
                case 2:
                    failInfo.AppendLine("Another logon or post-logon error occurred. The Remote Desktop client displays a logon screen to the user.");
                    break;
                case 1:
                    failInfo.AppendLine("The password is expired. The user must update their password to continue logging on.");
                    break;
                case 3:
                    failInfo.AppendLine("The Remote Desktop client displays a dialog box that contains important information for the user.");
                    break;
                case -1073741714:
                    failInfo.AppendLine("The user name and authentication information are valid, but authentication was blocked due to restrictions on the user account, such as time-of-day restrictions.");
                    break;
                case -1073741715:
                    failInfo.AppendLine("The attempted logon is not valid. This is due to either an incorrect user name or incorrect authentication information.");
                    break;
                case -1073741276:
                    failInfo.AppendLine("The password is expired. The user must update their password to continue logging on.");
                    break;
                default:
                    failInfo.AppendLine("An unknown error.");
                    break;
            }

            if (failCode == 3)
            {
                RaiseFormActionRequired(FormActionEventArgs.FormActionType.SendEnter);
            }
            else
            {
                if (failCode != -2)
                {
                    result = CheckStatusEnum.NotAuthentificated;
                    RaiseFormActionRequired(FormActionEventArgs.FormActionType.CaptureScreen);
                    //client.Disconnect();
                }
            }
        }

        private void client_OnLoginComplete(object sender, EventArgs e)
        {
            result = CheckStatusEnum.Authenticated;

            if (!sysInfoRequired)
            {
                client.Disconnect();
            }
        }

        private void client_OnDisconnected(object sender, IMsTscAxEvents_OnDisconnectedEvent e)
        {
            if (timer.Enabled)
                timer.Stop();

            if (e.discReason != 1 && e.discReason != 2 && e.discReason != 3)
            {
                //conection failure
                failCode = e.discReason;
                string failDescription = client.GetErrorDescription((uint)e.discReason, (uint)client.ExtendedDisconnectReason);
                failInfo.AppendFormat("Connection failed: {0}", string.IsNullOrEmpty(failDescription) ? "No description" : failDescription);
            }

            status = CheckerStatusEnum.done;
            SendCheckResult();
        }

        private void client_OnConnected(object sender, EventArgs e)
        {
            result = CheckStatusEnum.Connected;
            if (emptyPass)
            {
                RaiseFormActionRequired(FormActionEventArgs.FormActionType.SendEnter);
            }
        }

        public bool BeginCheck(string server, int? port, string username, string password, bool checkSystemInfo, string systemInfoParams, long cmdId)
        {
            if (status == CheckerStatusEnum.working)
            {     
                return false;
            }

            status = CheckerStatusEnum.working;
            failCode = null;
            failInfo.Clear();            
            id = Guid.NewGuid().ToString("N");
            sysInfoRequired = checkSystemInfo;
            commandId = cmdId;
            result = CheckStatusEnum.NotConnected;

            // domain check for DOMAIN\LOGIN
            string[] usernameParts = username.Split('\\');
            client.UserName = usernameParts.Length > 1 ? usernameParts[1] : usernameParts[0];
            client.Domain = usernameParts.Length > 1 ? usernameParts[0] : "";
            client.Server = server;
            
            if (port.HasValue)
            {
                client.AdvancedSettings2.RDPPort = port.Value;
            }

            emptyPass = string.IsNullOrEmpty(password);

            client.AdvancedSettings2.ClearTextPassword = emptyPass ? "" : password;                   
            client.AdvancedSettings2.RedirectPorts = false;
            client.AdvancedSettings2.RedirectPrinters = false;
            client.AdvancedSettings2.RedirectSmartCards = false;
            client.AdvancedSettings2.ConnectToServerConsole = true;
            client.AdvancedSettings5.AuthenticationLevel = 2; 

            if (checkSystemInfo)
            {
                client.AdvancedSettings2.RedirectDrives = true;
                client.AdvancedSettings2.singleConnectionTimeout = ConfigHelper.SysInfoConnectionTimeout;
                client.SecuredSettings.StartProgram = string.Format("cmd /c pushd {0} && siski.exe {1} >> {2}.txt", ConfigHelper.ResourceCheckerRemoteFolder, systemInfoParams, id);
            }
            else
            {
                client.AdvancedSettings2.RedirectDrives = false;
                client.AdvancedSettings2.singleConnectionTimeout = ConfigHelper.SimpleConnectionTimeout;
            }

            client.Connect();
            timer.Start();

            return true;
        }

        public void StopCheck()
        {
            if (status == CheckerStatusEnum.free)
            {
                return;
            }
        
            client.Disconnect();
        }

        public event EventHandler<CheckCompletedEventArgs> CheckCompleted;

        private void RaiseCheckCompleted(string systemInfo)
        {
            var handler = CheckCompleted;
            if (handler != null)
            {
                handler(this, new CheckCompletedEventArgs { CheckResult = result, SystemInfo = systemInfo, CmdId = commandId, FailInfo = failInfo.ToString(), FailCode = failCode });
            }
        }

        private void SendCheckResult()
        {
            string sysInfo = null;

            if (sysInfoRequired)
            {
                string sysInfoFile = Path.Combine(ConfigHelper.ResourceCheckerLocalFolder, id + ".txt");
                if (File.Exists(sysInfoFile))
                {
                    sysInfo = File.ReadAllText(sysInfoFile);
                    File.Delete(sysInfoFile);
                }
            }

            RaiseCheckCompleted(sysInfo);

            status = CheckerStatusEnum.free;
        }

        public event EventHandler<FormActionEventArgs> FormActionRequred;

        private void RaiseFormActionRequired(FormActionEventArgs.FormActionType actionType)
        {
            var handler = FormActionRequred;
            if (handler != null)
            {
                handler(this, new FormActionEventArgs { ActionType = actionType });
            }
        }
    }
}
