﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Checker.Client.Check
{
    public class FormActionEventArgs : EventArgs
    {
        public FormActionType ActionType { get; set; }

        public enum FormActionType
        {
            SendEnter,

            CaptureScreen
        }
    }
}
