﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Checker.DataContracts;

namespace Checker.Client.Check
{
    public interface IChecker
    {
        bool BeginCheck(string server, int? port, string username, string password, bool checkSystemInfo, string systemInfoParams, long cmdId);

        void StopCheck();

        event EventHandler<CheckCompletedEventArgs> CheckCompleted;

        CheckerStatusEnum Status { get; }

        event EventHandler<FormActionEventArgs> FormActionRequred;
    }
}
