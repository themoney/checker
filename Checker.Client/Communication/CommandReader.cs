﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net.Sockets;
using System.Threading;
using System.Net;
using Newtonsoft.Json;
using Checker.DataContracts;
using Checker.Client.Config;
using Checker.Logging;
using Newtonsoft.Json.Bson;

namespace Checker.Communication
{
    public class CommandReader : ICommandReader
    {
        private int localPort { get; set; }
        private Thread serverThread { get; set; }

        public CommandReader(int localPort)
        {
            this.localPort = localPort;
        }

        public void Start()
        {
            if (serverThread != null)
            {
                throw new InvalidOperationException("Server already started");
            }

            serverThread = new Thread(new ThreadStart(ServerListen));
            serverThread.IsBackground = true;
            serverThread.Start();
        }

        public void Stop()
        {
            if (serverThread != null)
            {
                serverThread.Abort();
                serverThread = null;
            }
        }  

        private void ServerListen()
        {
            Socket serverSocket = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
            serverSocket.Bind(new IPEndPoint(IPAddress.Any, localPort));
            serverSocket.Listen(1);

            while (true)
            {
                try
                {
                    Socket clientSocket = serverSocket.Accept();
                    using (NetworkStream stream = new NetworkStream(clientSocket))
                    {
                        var request = ReadRequest(stream);
                        OnNewCmdReceived(request);

                        stream.Close();
                    }

                    clientSocket.Close();
                }
                catch (Exception ex)
                {
                    Logger.AddError("Failed to process connection", ex);
                }
            }
        }

        private CmdRequest ReadRequest(NetworkStream stream)
        {
            CmdRequest request = null;
            JsonSerializer serializer = new JsonSerializer();
          
            using (JsonReader reader = new BsonReader(stream))
            {                                
                request = serializer.Deserialize<CmdRequest>(reader);
                reader.Close();
            }

            return request;
        }

        private void WriteReply(NetworkStream stream, CmdServerReply reply)
        {
            JsonSerializer serializer = new JsonSerializer();
            using (BsonWriter writer = new BsonWriter(stream))
            {
                serializer.Serialize(writer, reply);
                writer.Flush();
                writer.Close();
            }
        }

        public event EventHandler<NewCmdEventArgs> NewCmdReceived;

        private void OnNewCmdReceived(CmdRequest request)
        {
            if (request != null)
            {
                var handler = NewCmdReceived;
                if (handler != null)
                {
                    handler(this, new NewCmdEventArgs { Cmd = request });
                }
            }
        }

        public void SendToServer(CmdServerReply reply)
        {
            if (reply == null)
            {
                throw new ArgumentNullException("reply");
            }

            try
            {
                Socket sock = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
                sock.Connect("127.0.0.1", ConfigHelper.InternalServerPort);

                using (NetworkStream stream = new NetworkStream(sock))
                {
                    WriteReply(stream, reply);
                    stream.Close();
                }
            }
            catch (Exception ex)
            {
                Logger.AddError("Failed to send reply", ex);
            }
        }
    }
}
