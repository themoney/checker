﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Checker.DataContracts;
using System.Net.Sockets;

namespace Checker.Communication
{
    interface ICommandReader
    {
        void Start();

        void Stop();

        void SendToServer(CmdServerReply reply);

        event EventHandler<NewCmdEventArgs> NewCmdReceived;
    }
}
