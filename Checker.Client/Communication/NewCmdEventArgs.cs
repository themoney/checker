﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Checker.DataContracts;

namespace Checker.Communication
{
    public class NewCmdEventArgs : EventArgs
    {
        public CmdRequest Cmd { get; set; }
    }
}
