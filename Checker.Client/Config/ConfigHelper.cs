﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Configuration;

namespace Checker.Client.Config
{
    public static class ConfigHelper
    {
        public const int DefaultSysInfoConnectionTimeout = 600;

        public const int DefaultSimpleConnectionTimeout = 180;

        public const int DefaultCheckingTimeout = 600;

        public static int SysInfoConnectionTimeout
        {
            get
            {
                int r;
                if (int.TryParse(ConfigurationManager.AppSettings["SystemInfoConnectionTimeoutInSeconds"], out r))
                {
                    return r;
                }

                return DefaultSysInfoConnectionTimeout;
            }
        }

        public static int SimpleConnectionTimeout
        {
            get
            {
                int r;
                if (int.TryParse(ConfigurationManager.AppSettings["SimpleConnectionTimeoutInSeconds"], out r))
                {
                    return r;
                }

                return DefaultSimpleConnectionTimeout;
            }
        }

        public static int CheckingTimeout
        {
            get
            {
                int r;
                if (int.TryParse(ConfigurationManager.AppSettings["CheckingTimeoutInSeconds"], out r))
                {
                    return r;
                }

                return DefaultCheckingTimeout;
            }
        }

        public static int InternalServerPort
        {
            get
            {
                return int.Parse(ConfigurationManager.AppSettings["InternalServerPort"]);                
            }
        }

        public static string ResourceCheckerRemoteFolder
        {
            get
            {
                return ConfigurationManager.AppSettings["ResourceCheckerRemoteFolder"];
            }
        }

        public static string ResourceCheckerLocalFolder
        {
            get
            {
                return ConfigurationManager.AppSettings["ResourceCheckerLocalFolder"];
            }
        }

        public static string ResourceCheckerRemoteBatFolder
        {
            get
            {
                return ConfigurationManager.AppSettings["ResourceCheckerRemoteBatFolder"];
            }
        }
    }
}
