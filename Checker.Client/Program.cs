﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;

namespace Checker.Client
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            string[] cmdArgs = Environment.GetCommandLineArgs();
            if (cmdArgs.Length < 2)
            {
                throw new ArgumentNullException("Missing port parameter for the checker");
            }

            int checkerPort = 0;
            if (!int.TryParse(cmdArgs[1], out checkerPort))
            {
                throw new ArgumentOutOfRangeException("Failed to cast checker port argument");
            }

            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new frmCheck(checkerPort));
        }
    }
}
