﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Checker.Communication;
using Checker.Client.Check;
using Checker.DataContracts;
using System.Runtime.InteropServices;
using System.Threading;
using System.Diagnostics;
using Checker.Logging;
using System.IO;
using System.Drawing.Imaging;
using Checker.Client.Config;

namespace Checker.Client
{
    public partial class frmCheck : Form
    {
        private ICommandReader cmdReader { get; set; }
        private IChecker checker { get; set; }
        private Queue<CmdServer> serversQueue { get; set; }
        private byte[] captureBuf { get; set; }
        private int checkerPort { get; set; }

        public frmCheck(int checkerPort)
        {           
            InitializeComponent();

            this.checkerPort = checkerPort;

            checker = new Checker.Client.Check.Checker(this.rdpClient);
            checker.CheckCompleted += new EventHandler<CheckCompletedEventArgs>(checker_CheckCompleted);
            checker.FormActionRequred += new EventHandler<FormActionEventArgs>(checker_FormActionRequred);

            cmdReader = new CommandReader(checkerPort);
            cmdReader.NewCmdReceived += new EventHandler<NewCmdEventArgs>(cmdReader_NewCmdReceived);
            cmdReader.Start();

            serversQueue = new Queue<CmdServer>();
            this.Text = string.Format("CHECKER PORT:{0}", checkerPort);            
        }        

        //private void CaptureScreen()
        //{
        //    try
        //    {              
        //        using (Bitmap bmp = new Bitmap(this.Width, this.Height))
        //        {
        //            this.DrawToBitmap(bmp, new Rectangle(0, 0, this.Width, this.Height));
        //            using (MemoryStream memory = new MemoryStream())
        //            {
        //                bmp.Save(memory, ImageFormat.Jpeg);
        //                captureBuf = memory.GetBuffer();
        //            }
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        Logger.AddError("Failed to make screenshot", ex);
        //        captureBuf = null;
        //    }
        //}

        private void checker_FormActionRequred(object sender, FormActionEventArgs e)
        {
            if (e.ActionType == FormActionEventArgs.FormActionType.SendEnter)
            {
                Thread.Sleep(2000);
                IntPtr inputWindowHandle = GetInputWindowHandle();
                if (inputWindowHandle != IntPtr.Zero)
                {
                    SetForegroundWindow(Handle);
                    // sending ENTER
                    PostMessage(inputWindowHandle, 0x0100, 0x0D, 0x001C0001);
                    PostMessage(inputWindowHandle, 0x0101, 0x0D, 0x001C0001);
                }
            }
            else if (e.ActionType == FormActionEventArgs.FormActionType.CaptureScreen)
            {            
                //CaptureScreen();
                // Closing session so what Checker obj sends results
                this.rdpClient.Disconnect();
            }
        }

        private void checker_CheckCompleted(object sender, CheckCompletedEventArgs e)
        {
            CmdServerReply reply = new CmdServerReply();
            reply.id = e.CmdId;
            reply.status = e.CheckResult.ToString();
            reply.info = e.SystemInfo;            
            reply.code = e.FailCode;
            reply.fail = e.FailInfo;
            reply.port = this.checkerPort;

            //if (captureBuf != null)
            //{
            //    reply.fail_picture = new byte[captureBuf.Length];
            //    captureBuf.CopyTo(reply.fail_picture, 0);
            //    captureBuf = null;
            //}

            cmdReader.SendToServer(reply);

            PeekNewCmd();
        }

        private void cmdReader_NewCmdReceived(object sender, NewCmdEventArgs e)
        {
            var request = e.Cmd;
            if (request != null)
            {
                if (request.cmd == CmdTypeEnum.check.ToString() && request.servers != null && request.servers.Count > 0)
                {
                    lock (serversQueue)
                    {
                        request.servers.ForEach(x => serversQueue.Enqueue(x));
                    }

                    if (checker.Status != CheckerStatusEnum.working)
                    {
                        PeekNewCmd();
                    }
                }
                else if (request.cmd == CmdTypeEnum.kill.ToString())
                {
                    cmdReader.Stop();
                    checker.StopCheck();
                    Application.Exit();
                }
                else
                {
                    Logger.AddInformation(string.Format("Invalid command \"{0}\"", request.cmd));                    
                }
            }
        }

        private void PeekNewCmd()
        {
            lock (serversQueue)
            {
                if (serversQueue.Count > 0)
                {
                    CmdServer server = serversQueue.Dequeue();

                    if (!string.IsNullOrEmpty(server.info) && server.info.Contains("[path]"))
                    {
                        server.info = server.info.Replace("[path]", ConfigHelper.ResourceCheckerRemoteBatFolder);
                    }

                    if (!checker.BeginCheck(server.ip, server.port, server.login, server.pass, !string.IsNullOrEmpty(server.info), server.info, server.id))
                    {
                        // return server to queue if failed to start checking
                        serversQueue.Enqueue(server);
                    }
                }
            }
        }     

        private IntPtr GetInputWindowHandle()
        {
            List<IntPtr> windowHandles = new List<IntPtr>();         

            // fetching handle list of all process windows
            ProcessThreadCollection threads = Process.GetCurrentProcess().Threads;
            foreach (ProcessThread pt in threads)
            {
                EnumThreadWindows(pt.Id,
                    (hwnd, lParam) =>
                    {
                        windowHandles.Add(hwnd);

                        EnumChildWindows(hwnd, 
                            (h, l) => 
                            { 
                                windowHandles.Add(h); 
                                return true; 
                            }, 
                            IntPtr.Zero);

                        return true;
                    }, 
                    IntPtr.Zero);
            }

            foreach (IntPtr p in windowHandles)
            {
                int captionLen = GetWindowTextLength(p);
                if (captionLen > 0)
                {
                    StringBuilder sb = new StringBuilder(captionLen + 1);
                    GetWindowText(p, sb, sb.Capacity);

                    if (sb.ToString() == "Input Capture Window")
                    {
                        return p;
                    }
                }                
            }

            return IntPtr.Zero;
        }

        [DllImport("USER32.DLL")]
        public static extern bool SetForegroundWindow(IntPtr hWnd);

        [DllImport("user32.dll", SetLastError = true)]
        public static extern IntPtr PostMessage(IntPtr hWnd, uint uMsg, int wParam, int lParam);

        public delegate bool Win32Callback(IntPtr hWnd, IntPtr lParam);

        [DllImport("user32.dll")]
        public static extern bool EnumThreadWindows(int dwThreadId, Win32Callback lpfn, IntPtr lParam);

        [DllImport("user32.dll", CharSet = CharSet.Auto, SetLastError = true)]
        public static extern int GetWindowText(IntPtr hWnd, StringBuilder lpString, int nMaxCount);

        [DllImport("user32.Dll")]
        [return: MarshalAs(UnmanagedType.Bool)]
        public static extern bool EnumChildWindows(IntPtr parentHandle, Win32Callback callback, IntPtr lParam);

        [DllImport("user32.dll", SetLastError = true, CharSet = CharSet.Auto)]
        public static extern int GetWindowTextLength(IntPtr hWnd);
    }
}
