﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Checker.DataContracts
{
    public enum CheckStatusEnum
    {
        NotConnected,

        Connected,

        Authenticated,

        NotAuthentificated
    }
}
