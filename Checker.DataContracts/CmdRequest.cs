﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Newtonsoft.Json;

namespace Checker.DataContracts
{
    public class CmdRequest
    {        
        public string cmd { get; set; }
   
        public List<CmdServer> servers { get; set; }
    }
}
