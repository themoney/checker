﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Newtonsoft.Json;

namespace Checker.DataContracts
{
    public class CmdServer
    {
        public long id { get; set; }

        public string ip { get; set; }

        public int? port { get; set; }

        public string login { get; set; }

        public string pass { get; set; }

        public string info { get; set; }
    }
}
