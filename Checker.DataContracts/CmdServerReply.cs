﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Newtonsoft.Json;

namespace Checker.DataContracts
{
    public class CmdServerReply
    {
        public long id { get; set; }

        public string status { get; set; }        

        public string info { get; set; }

        public string fail { get; set; }

        public int? code { get; set; }

        public int port { get; set; }
    }
}
