﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Diagnostics;

namespace Checker.Logging
{
    public static class Logger
    {
        private const string ApplicationLogName = "Application";

        public static void AddInformation(string message)
        {
            if (EventLog.Exists(ApplicationLogName))
            {
                EventLog.WriteEntry(ApplicationLogName, message, EventLogEntryType.Information);
            }
        }

        public static void AddError(string message, Exception exception)
        {
            if (EventLog.Exists(ApplicationLogName))
            {
                StringBuilder sb = new StringBuilder();
                sb.AppendLine(message);

                Exception ex = exception;
                while (ex != null)
                {
                    sb.AppendLine("**********");
                    sb.AppendLine("msg:");
                    sb.AppendLine(ex.Message);
                    sb.AppendLine("----------");
                    sb.AppendLine("trace:");
                    sb.AppendLine(ex.StackTrace);
                    ex = ex.InnerException;
                }

                EventLog.WriteEntry(ApplicationLogName, sb.ToString(), EventLogEntryType.Error);
            }
        }
    }
}
