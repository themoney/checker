﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Checker.Server.CommandServer
{
    public class CmdProcessedEventArgs : EventArgs
    {
        public int ProcessedCmdCount { get; set; }
    }
}
