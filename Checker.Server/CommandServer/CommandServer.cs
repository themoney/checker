﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Net.Sockets;
using System.Net;
using Newtonsoft.Json.Bson;
using Newtonsoft.Json;
using Checker.DataContracts;
using Checker.Logging;
using System.IO;
using Checker.Database;
using Checker.Server.Config;

namespace Checker.Server.CommandServer
{
    public class CommandServer : ICommandServer
    {
        private bool stopFlag { get; set; }
        private int internalServerPort { get; set; }
        private Thread internalServerThread { get; set; }
        private Thread cmdThread { get; set; }
        private List<CmdServerReply> replies { get; set; }
        private int availableProcesses { get; set; }

        public CommandServer(int internalPort, int availableProcCount)
        {
            stopFlag = false;
            availableProcesses = availableProcCount;
            internalServerPort = internalPort;
            replies = new List<CmdServerReply>();
        }

        public void Start()
        {
            stopFlag = false;

            if (cmdThread != null)
            {
                throw new InvalidOperationException("External server already started");
            }

            if (internalServerThread != null)
            {
                throw new InvalidOperationException("Internal server already started");
            }

            cmdThread = new Thread(new ThreadStart(CmdListen));
            cmdThread.IsBackground = true;
            cmdThread.Start();

            internalServerThread = new Thread(new ThreadStart(InternalServerListen));
            internalServerThread.IsBackground = true;
            internalServerThread.Start();
        }

        public void Stop()
        {
            stopFlag = true;
            cmdThread = null;
            internalServerThread = null;
        }

        private void InternalServerListen()
        {
            Socket serverSocket = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
            serverSocket.Bind(new IPEndPoint(IPAddress.Any, internalServerPort));
            serverSocket.Listen(512);

            // connection loop
            while (!stopFlag)
            {
                try
                {
                    Socket clientSocket = serverSocket.Accept();
                    Thread clientTh = new Thread(new ParameterizedThreadStart(InternalClientThread));
                    clientTh.IsBackground = true;
                    clientTh.Start(clientSocket);
                }
                catch (Exception ex)
                {
                    Logger.AddError("Failed to handle checker connection", ex);
                }
            }
        }

        private void InternalClientThread(object socketObject)
        {
            CmdServerReply reply = null;
            Socket clientSocket = socketObject as Socket;
            if (clientSocket != null)
            {
                try
                {
                    using (NetworkStream stream = new NetworkStream(clientSocket))
                    {
                        using (BsonReader reader = new BsonReader(stream))
                        {
                            JsonSerializer serializer = new JsonSerializer();
                            reply = serializer.Deserialize<CmdServerReply>(reader);

                            reader.Close();
                        }

                        stream.Close();
                    }
                }
                catch (Exception ex)
                {
                    reply = null;
                    Logger.AddError("Failed to receive reply from the checker", ex);
                }
            }

            if (reply != null)
            {
                SendCmdReply(reply);
            }
        }

        private void CmdListen()
        {
            while (!stopFlag)
            {
                try
                {
                    using (CheckerEntities db = new CheckerEntities())
                    {
                        while (true)
                        {
                            ProcessReplies(db);
                            ProcessRequests(db);

                            Thread.Sleep(ConfigHelper.DatabasePollingTimeoutMiliseconds);
                        }
                    }
                }
                catch (Exception ex)
                {
                    if (!stopFlag)
                    {
                        // log exception
                        Logger.AddError("Database check thread failure", ex);
                    }
                }
            }
        }

        private void ProcessReplies(CheckerEntities db)
        {
            lock (replies)
            {
                if (replies.Count > 0)
                {
                    foreach (CmdServerReply reply in replies)
                    {
                        // notify thread server to free process
                        OnCmdReplyProcessed(reply);

                        // increasing availabel process counter
                        availableProcesses++;

                        var task = db.Tasks.FirstOrDefault(t => t.TaskId == reply.id);
                        if (task != null)
                        {
                            task.IsCompleted = true;

                            TaskResult result = new TaskResult
                            {
                                TaskId = task.TaskId,
                                Status = reply.status,
                                Info = string.IsNullOrEmpty(reply.info) ? string.Empty : reply.info,
                                ErrorCode = reply.code.HasValue ? reply.code.Value : 0,
                                ErrorMessage = reply.fail
                            };

                            db.TaskResults.AddObject(result);
                        }
                        else
                        {
                            Logger.AddInformation(string.Format("Failed to receive task from database with id = {0}", reply.id));
                        }
                    }

                    db.SaveChanges();

                    // updating processed reply count
                    OnCmdProcessed(replies.Count);

                    // cleaning replies
                    replies.Clear();
                }
            }
        }

        private void ProcessRequests(CheckerEntities db)
        {
            // if we have free processes
            if (availableProcesses > 0)
            {
                // getting not processed tasks
                var newTasks = db.Tasks.Where(t => !t.IsProcessed);
                if (newTasks != null && newTasks.Count() > 0)
                {
                    // take tasks according to free processes
                    newTasks = newTasks.Take(availableProcesses);

                    CmdRequest cmdRequest = new CmdRequest();
                    cmdRequest.cmd = CmdTypeEnum.check.ToString();
                    cmdRequest.servers = new List<CmdServer>();

                    foreach (var t in newTasks)
                    {
                        availableProcesses--;

                        t.IsProcessed = true;

                        CmdServer srv = new CmdServer
                        {
                            id = t.TaskId,
                            ip = t.Server.Trim(),
                            port = t.Port,
                            login = t.Login.Trim(),
                            pass = t.Password,
                            info = t.Info
                        };

                        cmdRequest.servers.Add(srv);
                    }

                    db.SaveChanges();

                    // notifying about new commands
                    OnNewCmd(cmdRequest);
                }
            }
        }


        public event EventHandler<NewCmdEventArgs> NewCmd;
        private void OnNewCmd(CmdRequest request)
        {
            if (request != null)
            {
                var handler = NewCmd;
                if (handler != null)
                {
                    handler(this, new NewCmdEventArgs { Request = request });
                }
            }
        }

        public event EventHandler<CmdProcessedEventArgs> CmdProcessed;
        private void OnCmdProcessed(int cmdCount)
        {
            var handler = CmdProcessed;
            if (handler != null)
            {
                handler(this, new CmdProcessedEventArgs { ProcessedCmdCount = cmdCount });
            }
        }

        private void SendCmdReply(CmdServerReply reply)
        {
            lock (replies)
            {
                replies.Add(reply);
            }
        }

        public event EventHandler<CmdReplyProcessedEventArgs> CmdReplyProcessed;
        private void OnCmdReplyProcessed(CmdServerReply reply)
        {
            var handler = CmdReplyProcessed;
            if (handler != null)
            {
                handler(this, new CmdReplyProcessedEventArgs { Reply = reply });
            }
        }
    }
}
