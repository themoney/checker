﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Checker.DataContracts;

namespace Checker.Server.CommandServer
{
    public interface ICommandServer
    {
        void Start();
        void Stop();

        event EventHandler<NewCmdEventArgs> NewCmd;

        event EventHandler<CmdProcessedEventArgs> CmdProcessed;

        event EventHandler<CmdReplyProcessedEventArgs> CmdReplyProcessed;
    }
}
