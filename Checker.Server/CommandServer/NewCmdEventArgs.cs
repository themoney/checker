﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Checker.DataContracts;

namespace Checker.Server.CommandServer
{
    public class NewCmdEventArgs : EventArgs
    {
        public CmdRequest Request { get; set; }
    }
}
