﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Configuration;

namespace Checker.Server.Config
{
    public static class ConfigHelper
    {
        public static int InternalServerPort
        {
            get
            {
                return int.Parse(ConfigurationManager.AppSettings["InternalServerPort"]);
            }
        }

        public static string CheckerFile
        {
            get
            {
                return ConfigurationManager.AppSettings["CheckerFile"];
            }
        }

        public static int CheckThreadCount
        {
            get
            {
                return int.Parse(ConfigurationManager.AppSettings["CheckThreadCount"]);
            }
        }

        public static int DatabasePollingTimeoutMiliseconds
        {
            get
            {
                return int.Parse(ConfigurationManager.AppSettings["DatabasePollingTimeoutMiliseconds"]);
            }
        }

        public static int LogWindowMaxLineCount
        {
            get
            {
                return int.Parse(ConfigurationManager.AppSettings["LogWindowMaxLineCount"]);
            }
        }

        public static string WindowCloseFile
        {
            get
            {
                return ConfigurationManager.AppSettings["WindowCloseFile"];
            }
        }
    }
}
