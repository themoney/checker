﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.IO;

namespace Checker.Server.Counter
{
    public partial class CounterForm : Form
    {
        public CounterForm()
        {
            InitializeComponent();
        }

        private int receivedCommandCount = 0;

        public int ReceivedCommandCount
        {
            get
            {
                return receivedCommandCount;
            }
            set
            {
                receivedCommandCount = value;
                Invoke((MethodInvoker)delegate { lblReceivedCount.Text = receivedCommandCount.ToString(); });
            }
        }

        private int processedCommandCount = 0;

        public int ProcessedCommandCount
        {
            get
            {
                return processedCommandCount;
            }
            set
            {
                processedCommandCount = value;
                Invoke((MethodInvoker)delegate { lblProcessedCount.Text = processedCommandCount.ToString(); });
            }
        }

        public void AddLog(string message)
        {
            Invoke((MethodInvoker)delegate
            {
                if (lbxLog.Items.Count > Config.ConfigHelper.LogWindowMaxLineCount)
                {
                    lbxLog.Items.Clear();
                }

                lbxLog.Items.Add(DateTime.Now.ToString("dd.MM.yyyy HH:mm:ss") + " " + message);
            });
        }
    }
}
