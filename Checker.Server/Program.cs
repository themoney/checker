﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Checker.Server.CommandServer;
using Checker.Server.ThreadServer;
using Checker.Server.Config;
using Checker.Logging;
using Checker.Server.Counter;
using System.Windows.Forms;
using System.Diagnostics;

namespace Checker.Server
{
    class Program
    {
        static Process wndCloseProcess { get; set; }
        static ICommandServer commandServer { get; set; }
        static IThreadServer threadServer { get; set; }
        static CounterForm counter { get; set; }

        [STAThread]
        static void Main(string[] args)
        {
            try
            {
                threadServer = new ThreadServer.ThreadServer(ConfigHelper.CheckThreadCount, ConfigHelper.CheckerFile);
                threadServer.TaskSent += new EventHandler<TaskSentEventArgs>(threadServer_TaskSent);
                threadServer.TaskSendFailed += new EventHandler<TaskSendFailedEventArgs>(threadServer_TaskSendFailed);
                Console.WriteLine("Starting checker processes...");
                threadServer.Start();
                Console.WriteLine("Checker processes started.");

                commandServer = new CommandServer.CommandServer(ConfigHelper.InternalServerPort, ConfigHelper.CheckThreadCount);
                commandServer.NewCmd += new EventHandler<NewCmdEventArgs>(commandServer_NewCmd);
                commandServer.CmdProcessed += new EventHandler<CmdProcessedEventArgs>(commandServer_CmdProcessed);
                commandServer.CmdReplyProcessed += new EventHandler<CmdReplyProcessedEventArgs>(commandServer_CmdReplyProcessed);
                Console.WriteLine(string.Format("Starting command server(internal port = {0})...", ConfigHelper.InternalServerPort));
                commandServer.Start();
                Console.WriteLine("Command server started.");

                Console.WriteLine("Starting window closer...");
                wndCloseProcess = new Process();
                wndCloseProcess.StartInfo.FileName = ConfigHelper.WindowCloseFile;

                if (wndCloseProcess.Start())
                    Console.WriteLine("Window closer started.");
                else
                    Console.WriteLine("Failed to start window closer.");

                Application.EnableVisualStyles();
                counter = new CounterForm();
                counter.FormClosed += new FormClosedEventHandler(counter_FormClosed);
                Application.Run(counter);
            }
            catch (Exception ex)
            {
                Logger.AddError("Main", ex);
            }
        }

        static void threadServer_TaskSendFailed(object sender, TaskSendFailedEventArgs e)
        {
            string logMsg = string.Format("Failed to send task {0}", e.TaskId);
            counter.AddLog(logMsg);
        }

        static void threadServer_TaskSent(object sender, TaskSentEventArgs e)
        {
            string logMsg = string.Format("Task {0} send to checker {1}", e.TaskId, e.ProcessPort);
            counter.AddLog(logMsg);
        }

        static void commandServer_CmdReplyProcessed(object sender, CmdReplyProcessedEventArgs e)
        {
            string logMsg = string.Format("Checker {0} completed task {1}", e.Reply.port, e.Reply.id);
            counter.AddLog(logMsg);

            threadServer.ProcessCmdReply(e.Reply.port);
        }

        static void counter_FormClosed(object sender, FormClosedEventArgs e)
        {
            wndCloseProcess.Kill();
            commandServer.Stop();
            threadServer.Stop();
        }

        static void commandServer_CmdProcessed(object sender, CmdProcessedEventArgs e)
        {
            counter.ProcessedCommandCount += e.ProcessedCmdCount;
        }

        static void commandServer_NewCmd(object sender, NewCmdEventArgs e)
        {
            try
            {
                if (e.Request != null && e.Request.servers != null)
                {
                    counter.ReceivedCommandCount += e.Request.servers.Count;

                    StringBuilder sb = new StringBuilder();
                    sb.Append("New tasks received (");
                    e.Request.servers.ForEach(s => sb.Append(s.id));
                    sb.Append(")");
                    counter.AddLog(sb.ToString());
                }

                threadServer.ProcessCmdRequest(e.Request);
            }
            catch (Exception ex)
            {
                Logger.AddError("Unable process cmd request", ex);
            }
        }
    }
}
