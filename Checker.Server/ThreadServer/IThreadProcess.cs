﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Diagnostics;
using Checker.DataContracts;

namespace Checker.Server.ThreadServer
{
    public interface IThreadProcess
    {
        int Port { get; set; }

        Process Process { get; set; }

        bool SendCmdRequest(CmdRequest request);

        bool Bussy { get; set; }
    }
}
