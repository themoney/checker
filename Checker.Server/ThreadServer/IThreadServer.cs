﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Checker.DataContracts;

namespace Checker.Server.ThreadServer
{
    public interface IThreadServer
    {
        void Start();

        void Stop();

        void ProcessCmdRequest(CmdRequest request);

        void ProcessCmdReply(int processPort);

        event EventHandler<TaskSentEventArgs> TaskSent;

        event EventHandler<TaskSendFailedEventArgs> TaskSendFailed;
    }
}
