﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Checker.Server.ThreadServer
{
    public class TaskSendFailedEventArgs : EventArgs
    {
        public long TaskId { get; set; }
    }
}
