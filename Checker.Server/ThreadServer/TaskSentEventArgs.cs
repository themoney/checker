﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Checker.Server.ThreadServer
{
    public class TaskSentEventArgs : EventArgs
    {
        public int ProcessPort { get; set; }

        public long TaskId { get; set; }
    }
}
