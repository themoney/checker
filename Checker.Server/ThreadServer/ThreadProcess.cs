﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Diagnostics;
using Checker.DataContracts;
using System.Net.Sockets;
using Newtonsoft.Json.Bson;
using Newtonsoft.Json;
using Checker.Logging;

namespace Checker.Server.ThreadServer
{
    public class ThreadProcess : IThreadProcess
    {
        public int Port { get; set; }

        public Process Process { get; set; }

        public bool SendCmdRequest(CmdRequest request)
        {
            Socket socket = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);

            try
            {
                socket.Connect("127.0.0.1", Port);
                using (NetworkStream stream = new NetworkStream(socket))
                {
                    using (BsonWriter writer = new BsonWriter(stream))
                    {
                        JsonSerializer serializer = new JsonSerializer();
                        serializer.Serialize(writer, request);
                        writer.Flush();
                        writer.Close();
                    }
                   
                    stream.Close();
                }
            }
            catch(Exception ex)
            {
                Logger.AddError(string.Format("Failed to send cmd request to checker with port = {0}", Port), ex);
                return false;
            }

            return true;
        }

        public bool Bussy { get; set; }
    }
}
