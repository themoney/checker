﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Diagnostics;
using System.IO;
using Checker.DataContracts;
using Checker.Logging;

namespace Checker.Server.ThreadServer
{
    public class ThreadServer : IThreadServer
    {
        private int currentProcess { get; set; }
        private const int ProcessPortBase = 1000;
        private const int ProcessCmdRequestAttemptCount = 10;

        private IThreadProcess[] processes { get; set; }
        private string threadFile { get; set; }

        public ThreadServer(int threadCount, string threadFile)
        {
            processes = new ThreadProcess[threadCount];
            this.threadFile = threadFile;
            currentProcess = 0;
        }

        public void Start()
        {
            if (string.IsNullOrEmpty(threadFile))
            {
                throw new ArgumentNullException("Missing thread file argument");
            }

            if (!File.Exists(threadFile))
            {
                throw new InvalidOperationException("Thread file doesn't exists");
            }

            for (int i = 0; i < processes.Length; i++)
            {
                ThreadProcess proc = new ThreadProcess();
                proc.Port = ProcessPortBase + i;
                proc.Process = new Process();
                proc.Process.StartInfo.FileName = threadFile;
                proc.Process.StartInfo.Arguments = proc.Port.ToString();
                if (!proc.Process.Start())
                {
                    throw new InvalidOperationException(string.Format("Can not start thread with path \"{0}\"", threadFile));
                }

                processes[i] = proc;
            }
        }

        public void Stop()
        {
            foreach (ThreadProcess p in processes)
            {
                p.Process.Kill();
            }
        }

        public void ProcessCmdRequest(CmdRequest request)
        {
            if (request == null)
            {
                return;
            }

            if (request.cmd == CmdTypeEnum.check.ToString())
            {
                // get all free processes
                var freeProcesses = processes.Where(p => !p.Bussy).ToArray();

                // loop throught tasks
                foreach (CmdServer server in request.servers)
                {
                    int i = 0;
                    for (i = 0; i < ProcessCmdRequestAttemptCount; i++)
                    {
                        if (currentProcess >= freeProcesses.Length)
                        {
                            currentProcess = 0;
                        }

                        IThreadProcess proc = freeProcesses[currentProcess++];
                        if (proc != null)
                        {
                            if (proc.SendCmdRequest(new CmdRequest { cmd = CmdTypeEnum.check.ToString(), servers = new List<CmdServer> { server } }))
                            {
                                OnTaskSent(server.id, proc.Port);
                                proc.Bussy = true;
                                break;
                            }
                        }
                    }

                    if (i >= ProcessCmdRequestAttemptCount)
                    {
                        OnTaskSendFailed(server.id);
                        Logger.AddError(string.Format("Failed to send cmd request to thread process with server id = {0}", server.id), new InvalidOperationException());
                    }
                }
            }
            else if (request.cmd == CmdTypeEnum.restart.ToString())
            {
                Stop();
                Start();
            }
            else if (request.cmd == CmdTypeEnum.kill.ToString())
            {
                Stop();
            }
        }

        public void ProcessCmdReply(int processPort)
        {
            var proc = processes.FirstOrDefault(p => p.Port == processPort);
            if (proc != null)
            {
                proc.Bussy = false;
            }
        }

        public event EventHandler<TaskSentEventArgs> TaskSent;
        private void OnTaskSent(long taskId, int processPort)
        {
            var handler = TaskSent;
            if (handler != null)
            {
                handler(this, new TaskSentEventArgs { TaskId = taskId, ProcessPort = processPort });
            }
        }

        public event EventHandler<TaskSendFailedEventArgs> TaskSendFailed;
        private void OnTaskSendFailed(long taskId)
        {
            var handler = TaskSendFailed;
            if (handler != null)
            {
                handler(this, new TaskSendFailedEventArgs { TaskId = taskId });
            }
        }
    }
}
