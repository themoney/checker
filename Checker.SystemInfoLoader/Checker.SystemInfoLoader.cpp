// Checker.SystemInfoLoader.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include <WinSock2.h>
#include <Windows.h>
#include <Shlobj.h>
#include <Shlwapi.h>

#include <iostream>
#include <fstream>

using namespace std;

#pragma comment(lib, "ws2_32")
#pragma comment(lib, "Shlwapi")

#define WAIT_TIME 300000

#define DOWNLOAD_BUF_SIZE 4194304
#define DOWNLOAD_TIME_S 10
#define DOWNLOAD_TIME_MS DOWNLOAD_TIME_S * 1000
#define DOWNLOAD_THREAD_COUNT 5
#define DOWNLOAD_PORT 1502

#define UPLOAD_BUF_MAX_SIZE 4194304
#define UPLOAD_BUF_MIN_SIZE 102400
#define UPLOAD_BUF_STEP_BYTES 102400
#define UPLOAD_THREAD_COUNT 4
#define UPLOAD_TIME_S 10
#define UPLOAD_TIME_MS UPLOAD_TIME_S * 1000
#define UPLOAD_PORT 1501

#define NAT_SERVER_PORT 1503
#define NAT_PORT 39393
#define NAT_PORT_STRING "39393"

void PrintProcessorName();
void PrintMemorySize();
void PrintWindowsVersion();
void PrintIsUserAdmin();
void PrintUptime();
HANDLE FindInRegistry(char* value);
bool IsEqual(char* a, char* b);
HANDLE PrintBroadband(char* server);
void ExecuteBat(char* batFile);
void Logoff();
void PrintComputerId();
HANDLE SearchCookie(char* cookie);
HANDLE CheckRegistryKey(char* keyFile);
bool StartsWith(char* src, char* part);
void WriteLine(const char* format, ...);

WSADATA wsa;
CRITICAL_SECTION cs;

int _tmain(int argc, _TCHAR* argv[])
{
	InitializeCriticalSection(&cs);

	PrintComputerId();	

	int waitHandleCount = 0;
	HANDLE* waitHandles = (HANDLE*)malloc(sizeof(HANDLE) * 100);
	bool systemInfo = false;

	for(int i=0; i<argc; i++)
	{
		if(IsEqual(argv[i], "--sys"))
		{			
			PrintProcessorName();
			PrintMemorySize();
			PrintWindowsVersion();
		}
		else if(IsEqual(argv[i], "--adm"))
		{
			PrintIsUserAdmin();
		}
		else if(IsEqual(argv[i], "--uptime"))
		{
			PrintUptime();
		}
		else if(IsEqual(argv[i], "--reg"))
		{			
			i++; // move to parameter		

			fstream file;
			file.open(argv[i]);

			if(!file.is_open())
			{
				WriteLine("registry: failed to open file \"%s\"\n", argv[i]);
				continue;
			}

			while(!file.eof())
			{		
				char* regBuf = (char*)malloc(100);
				file.getline(regBuf, 100);				
				HANDLE searchHandle = FindInRegistry(regBuf);
				waitHandles[waitHandleCount++] = searchHandle;
			}

			file.close();
		}
		else if(IsEqual(argv[i], "--all"))
		{
			PrintProcessorName();
			PrintMemorySize();
			PrintWindowsVersion();
			PrintIsUserAdmin();
			PrintUptime();
		}
		else if(IsEqual(argv[i], "--net"))
		{
			WSAStartup(0x0202, &wsa);

			i++; // move to parameter
			HANDLE broadbandHandle = PrintBroadband(argv[i]);
			waitHandles[waitHandleCount++] = broadbandHandle;

		}
		else if(IsEqual(argv[i], "--exec"))
		{
			i++; // move to parameter
			ExecuteBat(argv[i]);
		}
		else if(IsEqual(argv[i], "--logout"))
		{
			Logoff();
		}
		else if(IsEqual(argv[i], "--cookie"))
		{
			i++; // move to parameter

			fstream file;
			file.open(argv[i]);

			if(!file.is_open())
			{
				WriteLine("cookie: failed to open file \"%s\"\n", argv[i]);
				continue;
			}

			while(!file.eof())
			{		
				char* cookieBuf = (char*)malloc(100);
				file.getline(cookieBuf, 100);
				HANDLE cookieHandle = SearchCookie(cookieBuf);
				waitHandles[waitHandleCount++] = cookieHandle;
			}

			file.close();			
		}
		else if(IsEqual(argv[i], "--systeminfo"))
		{
			systemInfo = true;
		}
		else if(IsEqual(argv[i], "--regpath"))
		{
			i++;
			HANDLE keyCheckHandle = CheckRegistryKey(argv[i]);
			waitHandles[waitHandleCount++] = keyCheckHandle;
		}
	}	

	if(waitHandleCount > 0)
	{
		WaitForMultipleObjects(waitHandleCount, waitHandles, TRUE, WAIT_TIME);
		for(int i=0; i<waitHandleCount; i++)
		{
			CloseHandle(waitHandles[i]);
		}

		free(waitHandles);
	}

	if(systemInfo)
		system("systeminfo");

	return 0;
}

void PrintProcessorName()
{
	SYSTEM_INFO sysInfo;
	DWORD bufLen = 100;
	char buf[100];
	HKEY hKey;

	GetSystemInfo(&sysInfo);

	if(RegOpenKeyEx(HKEY_LOCAL_MACHINE, "HARDWARE\\DESCRIPTION\\System\\CentralProcessor\\0", 0, KEY_READ, &hKey) == ERROR_SUCCESS)
	{
		if(RegQueryValueEx(hKey, "ProcessorNameString", NULL, NULL, (BYTE*)&buf, &bufLen) == ERROR_SUCCESS)
		{
			WriteLine("cpu:%s\ncores:%d\n", buf, sysInfo.dwNumberOfProcessors);
		}
		else
		{
			WriteLine("cpu:unknown\n");
		}

		RegCloseKey(hKey);
	}
}

void PrintMemorySize()
{
	MEMORYSTATUSEX memoryStatus;

	memoryStatus.dwLength = sizeof(memoryStatus);
	GlobalMemoryStatusEx(&memoryStatus);
	WriteLine("memory:%d\n", (memoryStatus.ullTotalPhys / (1024*1024*1024)) + 1);
}

void PrintWindowsVersion()
{
	OSVERSIONINFOEX version;
	SYSTEM_INFO sysInfo;

	version.dwOSVersionInfoSize = sizeof(version);
	GetVersionEx((OSVERSIONINFO *)&version);

	GetSystemInfo(&sysInfo);

	if((version.dwMajorVersion == 5) && (version.dwMinorVersion == 0)) WriteLine("os:Win2000\n");
	else if((version.dwMajorVersion == 5) && (version.dwMinorVersion == 1)) WriteLine("os:WinXP\n");
	else if((version.dwMajorVersion == 5) && (version.dwMinorVersion == 2))
	{
		if((version.wProductType == VER_NT_WORKSTATION) && (sysInfo.wProcessorArchitecture == PROCESSOR_ARCHITECTURE_AMD64)) WriteLine("os:WinXP_x64\n");
		else if(GetSystemMetrics(SM_SERVERR2) == 0) WriteLine("os:Win2003\n");
		else if(version.wSuiteMask & VER_SUITE_WH_SERVER) WriteLine("os:WinHomeServer\n");
		else if(GetSystemMetrics(SM_SERVERR2) != 0) WriteLine("os:Win2003R2\n");
		else WriteLine("os:Win2003\n");
	}
	else if((version.dwMajorVersion == 6) && (version.dwMinorVersion == 0))
	{
		if(version.wProductType == VER_NT_WORKSTATION) WriteLine("os:WinVista\n");
		else WriteLine("os:Win2008\n");
	}
	else if((version.dwMajorVersion == 6) && (version.dwMinorVersion == 1))
	{
		if(version.wProductType == VER_NT_WORKSTATION) WriteLine("os:Win7\n");
		else WriteLine("os:Win2008R2\n");
	}
	else WriteLine("os:unknown\n");	
}

void PrintIsUserAdmin()
{
	SID_IDENTIFIER_AUTHORITY NtAuthority = SECURITY_NT_AUTHORITY;
	PSID AdministratorsGroup = NULL; 
	BOOL isAdmin = FALSE;

	if(AllocateAndInitializeSid(&NtAuthority, 2, SECURITY_BUILTIN_DOMAIN_RID, DOMAIN_ALIAS_RID_ADMINS, 0, 0, 0, 0, 0, 0, &AdministratorsGroup)) 
	{
		if (CheckTokenMembership( NULL, AdministratorsGroup, &isAdmin)) 
		{
			WriteLine("admin:true\n");
		}

		FreeSid(AdministratorsGroup); 
	}
	else
	{
		WriteLine("admin:false\n");
	}
}


#define TOTALBYTES    8192
#define BYTEINCREMENT 1024

PPERF_OBJECT_TYPE FirstObject(PPERF_DATA_BLOCK PerfData)
{
	return( (PPERF_OBJECT_TYPE)((PBYTE)PerfData + PerfData->HeaderLength) );
}

PPERF_OBJECT_TYPE NextObject(PPERF_OBJECT_TYPE PerfObj)
{
	return( (PPERF_OBJECT_TYPE)((PBYTE)PerfObj + PerfObj->TotalByteLength) );
}

PPERF_INSTANCE_DEFINITION FirstInstance(PPERF_OBJECT_TYPE PerfObj)
{
	return( (PPERF_INSTANCE_DEFINITION)((PBYTE)PerfObj + PerfObj->DefinitionLength) );
}

PPERF_INSTANCE_DEFINITION NextInstance(PPERF_INSTANCE_DEFINITION PerfInst)
{
	PPERF_COUNTER_BLOCK PerfCntrBlk;
	PerfCntrBlk = (PPERF_COUNTER_BLOCK)((PBYTE)PerfInst + PerfInst->ByteLength);
	return( (PPERF_INSTANCE_DEFINITION)((PBYTE)PerfCntrBlk + PerfCntrBlk->ByteLength) );
}

PPERF_COUNTER_DEFINITION FirstCounter(PPERF_OBJECT_TYPE PerfObj)
{
	return( (PPERF_COUNTER_DEFINITION) ((PBYTE)PerfObj + PerfObj->HeaderLength) );
}

PPERF_COUNTER_DEFINITION NextCounter(PPERF_COUNTER_DEFINITION PerfCntr)
{
	return( (PPERF_COUNTER_DEFINITION)((PBYTE)PerfCntr + PerfCntr->ByteLength) );
}

void PrintUptime()
{
	PPERF_DATA_BLOCK PerfData = NULL;
	PPERF_OBJECT_TYPE PerfObj;
	PPERF_COUNTER_DEFINITION PerfCntr;
	PPERF_COUNTER_BLOCK PtrToCntr;
	DWORD BufferSize = TOTALBYTES;
	DWORD j;
	LONGLONG result = -1;

	PerfData = (PPERF_DATA_BLOCK) malloc( BufferSize );

	while( RegQueryValueEx( HKEY_PERFORMANCE_DATA, "2", NULL, NULL, (LPBYTE) PerfData, &BufferSize ) == ERROR_MORE_DATA )
	{
		BufferSize += BYTEINCREMENT;
		PerfData = (PPERF_DATA_BLOCK) realloc( PerfData, BufferSize );
	}

	PerfObj = FirstObject(PerfData);
	PerfCntr = FirstCounter(PerfObj);
	PtrToCntr = (PPERF_COUNTER_BLOCK) ((PBYTE)PerfObj + PerfObj->DefinitionLength);

	for( j=0; j < PerfObj->NumCounters; j++ )
	{
		if (PerfCntr->CounterNameTitleIndex == 674)
		{
			LONGLONG x,y,tb;
			LPVOID pb = (LPVOID)((PBYTE)PtrToCntr + PerfCntr->CounterOffset);
			x = *(LONGLONG*)pb;
			y = (LONGLONG)PerfObj->PerfTime.QuadPart;
			tb = (LONGLONG)PerfObj->PerfFreq.QuadPart;
			result = (y - x)/tb;
			break;
		}

		PerfCntr = NextCounter(PerfCntr);		
	}

	free(PerfData);

	WriteLine("uptime:%lld\n", result);
}

typedef struct REGISTRY_SEARCH
{
	HKEY hKey;
	char* Path;
	char* SearchValue;
	bool Found;
	bool IsHex;
};

char* ByteToHex(BYTE* byteArray, DWORD arrayLength)
{
	if(arrayLength <= 0)
		return NULL;

	char* result = (char*)malloc(arrayLength * 2 + 1);
	result[0] = 0;
	
	for(int i = 0, j = 0; i<arrayLength; i++, j+=2)
	{
		sprintf(&result[j], "%02X\0", byteArray[i]);
	}

	return result;
}

bool IterateRegistryValues(REGISTRY_SEARCH* search, DWORD valueCount, DWORD valueMaxLength, DWORD valueDataMaxLength)
{
	bool result = false;
	DWORD valueType = 0;
	DWORD valueLength = 0;
	DWORD valueDataLength = 0;

	char* valueTmpBuf = (char*)malloc(valueMaxLength + 1);
	char* valueDataTmpBuf = (char*)malloc(valueDataMaxLength + 1);

	for(DWORD i=0; i<valueCount; i++)
	{
		valueLength = valueMaxLength;
		valueDataLength = valueDataMaxLength;

		if(RegEnumValue(search->hKey, i, valueTmpBuf, &valueLength, NULL, &valueType, (BYTE*)valueDataTmpBuf, &valueDataLength) == ERROR_SUCCESS)
		{
			if(!search->IsHex && (valueType == REG_SZ || valueType == REG_MULTI_SZ || valueType == REG_EXPAND_SZ))
			{
				if(StrStrI(valueDataTmpBuf, search->SearchValue) != NULL)
					result = true;
			}
			else if(search->IsHex && valueType == REG_BINARY)
			{
				char* hexData = ByteToHex((BYTE*)valueDataTmpBuf, valueDataLength);
				if(hexData != NULL)
				{
					if(StrStrI(hexData, search->SearchValue) != NULL)
						result = true;

					free(hexData);
				}
			}
		}

		if(result)
			break;
	}

	free(valueTmpBuf);
	free(valueDataTmpBuf);

	return result;
}

DWORD WINAPI RegistryThread(void* p)
{
	REGISTRY_SEARCH* search = (REGISTRY_SEARCH*)p;

	DWORD subKeyCount = 0;
	DWORD subKeyMaxLength = 0;
	DWORD valueCount = 0;
	DWORD valueMaxLength = 0;
	DWORD valueDataMaxLength = 0;
	HKEY hSubKey;

	if(RegQueryInfoKey(search->hKey, NULL, NULL, NULL, &subKeyCount, &subKeyMaxLength, NULL, &valueCount, 
		&valueMaxLength, &valueDataMaxLength, NULL,  NULL) != ERROR_SUCCESS)
		return 1;		

	// iterating key values
	if(!IterateRegistryValues(search, valueCount, valueMaxLength, valueDataMaxLength))
	{
		char* subKeyTmpBuf = (char*)malloc(subKeyMaxLength + 1);

		// iterating key subkeys
		for(DWORD i=0; i<subKeyCount && !search->Found; i++)
		{
			if(RegEnumKey(search->hKey, i, subKeyTmpBuf, subKeyMaxLength) == ERROR_SUCCESS)
			{
				if(!search->IsHex && (StrStrI(subKeyTmpBuf, search->SearchValue) != NULL))
				{
					search->Found = true;
					break;
				}

				if(RegOpenKeyEx(search->hKey, subKeyTmpBuf, 0, KEY_READ, &hSubKey) == ERROR_SUCCESS)
				{
					// making new search structure
					REGISTRY_SEARCH* subKeySearch = (REGISTRY_SEARCH*)malloc(sizeof(REGISTRY_SEARCH));
					subKeySearch->hKey = hSubKey;	
					subKeySearch->SearchValue = (char*)malloc(strlen(search->SearchValue) + 1);
					strcpy(subKeySearch->SearchValue, search->SearchValue);
					subKeySearch->Path = (char*)malloc(strlen(search->Path) + strlen(subKeyTmpBuf) + 2);
					strcpy(subKeySearch->Path, search->Path);
					strcat(subKeySearch->Path, "\\");
					strcat(subKeySearch->Path, subKeyTmpBuf);
					subKeySearch->Found = false;	
					subKeySearch->IsHex = search->IsHex;

					RegistryThread(subKeySearch);

					search->Found = subKeySearch->Found;
					free(subKeySearch);
				}
			}
		}

		free(subKeyTmpBuf);
	}
	else
	{
		search->Found = true;
	}
		
	RegCloseKey(search->hKey);
	free(search->Path);
	free(search->SearchValue);	

	return 0;
}

DWORD WINAPI RegSearchThread(void* p)
{
	if(p == NULL)
		return 0;

	char* originalValue = (char*)p;
	if(strlen(originalValue) == 0)
		return 0;

	char* value = (char*)malloc(strlen(originalValue) + 1);
	strcpy(value, originalValue);

	long hexVal = 0;
	bool isHex = false;
	char* sepparator = StrStrI(value, "#HEX#");
	if(sepparator != NULL)
	{		
		isHex = true;
		*sepparator = 0;		
	}

	HKEY hSearchKey;
	HANDLE* SearchHandles = (HANDLE*)malloc(sizeof(HANDLE) * 4);
	int handleIndex = 0;
	REGISTRY_SEARCH** searches = (REGISTRY_SEARCH**)malloc(sizeof(REGISTRY_SEARCH) * 4);

	if(RegOpenKeyEx(HKEY_LOCAL_MACHINE, NULL, 0, KEY_READ, &hSearchKey) == ERROR_SUCCESS)
	{	
		searches[handleIndex] = (REGISTRY_SEARCH*)malloc(sizeof(REGISTRY_SEARCH));
		searches[handleIndex]->Path = (char*)malloc(5);
		strcpy(searches[handleIndex]->Path, "HKLM");	
		searches[handleIndex]->SearchValue = (char*)malloc(strlen(value) + 1);
		strcpy(searches[handleIndex]->SearchValue, value);
		searches[handleIndex]->hKey = hSearchKey;
		searches[handleIndex]->Found = false;
		searches[handleIndex]->IsHex = isHex;

		SearchHandles[handleIndex++] = CreateThread(NULL, NULL, RegistryThread, searches[handleIndex], NULL, NULL);
	}
	
	if(RegOpenKeyEx(HKEY_CURRENT_USER, NULL, 0, KEY_READ, &hSearchKey) == ERROR_SUCCESS)
	{
		searches[handleIndex] = (REGISTRY_SEARCH*)malloc(sizeof(REGISTRY_SEARCH));
		searches[handleIndex]->Path = (char*)malloc(5);
		strcpy(searches[handleIndex]->Path, "HKCU");	
		searches[handleIndex]->SearchValue = (char*)malloc(strlen(value) + 1);
		strcpy(searches[handleIndex]->SearchValue, value);
		searches[handleIndex]->hKey = hSearchKey;
		searches[handleIndex]->Found = false;
		searches[handleIndex]->IsHex = isHex;

		SearchHandles[handleIndex++] = CreateThread(NULL, NULL, RegistryThread, searches[handleIndex], NULL, NULL);
	}

	if(RegOpenKeyEx(HKEY_CLASSES_ROOT, NULL, 0, KEY_READ, &hSearchKey) == ERROR_SUCCESS)
	{		
		searches[handleIndex] = (REGISTRY_SEARCH*)malloc(sizeof(REGISTRY_SEARCH));
		searches[handleIndex]->Path = (char*)malloc(5);
		strcpy(searches[handleIndex]->Path, "HKCR");	
		searches[handleIndex]->SearchValue = (char*)malloc(strlen(value) + 1);
		strcpy(searches[handleIndex]->SearchValue, value);	
		searches[handleIndex]->hKey = hSearchKey;
		searches[handleIndex]->Found = false;
		searches[handleIndex]->IsHex = isHex;

		SearchHandles[handleIndex++] = CreateThread(NULL, NULL, RegistryThread, searches[handleIndex], NULL, NULL);
	}

	if(RegOpenKeyEx(HKEY_USERS, NULL, 0, KEY_READ, &hSearchKey) == ERROR_SUCCESS)
	{
		searches[handleIndex] = (REGISTRY_SEARCH*)malloc(sizeof(REGISTRY_SEARCH));
		searches[handleIndex]->Path = (char*)malloc(4);
		strcpy(searches[handleIndex]->Path, "HKU");	
		searches[handleIndex]->SearchValue = (char*)malloc(strlen(value) + 1);
		strcpy(searches[handleIndex]->SearchValue, value);
		searches[handleIndex]->hKey = hSearchKey;
		searches[handleIndex]->Found = false;
		searches[handleIndex]->IsHex = isHex;

		SearchHandles[handleIndex++] = CreateThread(NULL, NULL, RegistryThread, searches[handleIndex], NULL, NULL);
	}		

	bool found = false;
	WaitForMultipleObjects(handleIndex, SearchHandles, TRUE, WAIT_TIME);

	for(int i=0; i<handleIndex; i++)
	{		
		CloseHandle(SearchHandles[i]);

		if(searches[i]->Found)
			found = true;

		free(searches[i]);
	}

	free(searches);
	free(SearchHandles);

	WriteLine("registry %s: %s\n", value, found ? "true" : "false");

	free(value);
	free(originalValue);
		
	return 0;
}

HANDLE FindInRegistry(char* value)
{
	return CreateThread(NULL, NULL, RegSearchThread, value, NULL, NULL);
}

bool IsEqual(char* a, char* b)
{
	return strcmpi(a, b) == 0;
}

bool StartsWith(char* src, char* part)
{
	return strstr(src, part) == src;
}

typedef struct DOWNLOAD_UPLOAD_PARAMS
{
	SOCKET sock;
	DWORD length;
	bool stop;
	int count;
};

DWORD WINAPI DownloadLoopThread(void* p)
{
	LARGE_INTEGER frequency, start, end;
	DOWNLOAD_UPLOAD_PARAMS* params = (DOWNLOAD_UPLOAD_PARAMS*)p;
	char* downloadBuf = (char*)malloc(DOWNLOAD_BUF_SIZE);
	DWORD downloadedBytes = 0;
	double elapsedTime = 0.0;

	QueryPerformanceFrequency(&frequency);
	LONGLONG freq = frequency.QuadPart;

	while(!params->stop)
	{		
		QueryPerformanceCounter(&start);
		int downloaded = recv(params->sock, downloadBuf, DOWNLOAD_BUF_SIZE, 0);
		QueryPerformanceCounter(&end);

		if(downloaded == SOCKET_ERROR)
		{
			WriteLine("download failed\n");
			break;
		}

		LONGLONG dif = end.QuadPart - start.QuadPart;		
		elapsedTime += (double)dif / (double)freq;
		downloadedBytes += downloaded;

		if(elapsedTime >= 1.0)
		{				
			params->length += downloadedBytes / elapsedTime;
			params->count++;

			downloadedBytes = 0;
			elapsedTime = 0.0;
		}
	}

	closesocket(params->sock);

	free(downloadBuf);

	return 0;
}

DWORD WINAPI DownloadThread(void* p)
{
	DOWNLOAD_UPLOAD_PARAMS* downloadParamas = (DOWNLOAD_UPLOAD_PARAMS*)malloc(sizeof(DOWNLOAD_UPLOAD_PARAMS) * DOWNLOAD_THREAD_COUNT);	

	sockaddr_in dest_addr;
	dest_addr.sin_family = AF_INET;
	dest_addr.sin_port = htons(DOWNLOAD_PORT);

	HOSTENT* host = gethostbyname((char*)p);

	if(host != NULL)	
		dest_addr.sin_addr.s_addr = *(ULONG*)host->h_addr_list[0];	
	else	
		dest_addr.sin_addr.s_addr = inet_addr((char*)p);

	HANDLE* downloadThreads = (HANDLE*)malloc(sizeof(HANDLE) * DOWNLOAD_THREAD_COUNT);
	for(int i=0; i<DOWNLOAD_THREAD_COUNT; i++) // making download threads
	{
		downloadParamas[i].length = 0;
		downloadParamas[i].stop = false;
		downloadParamas[i].sock = socket(AF_INET, SOCK_STREAM, 0);
		downloadParamas[i].count = 0;
		connect(downloadParamas[i].sock, (sockaddr*)&dest_addr, sizeof(dest_addr));

		downloadThreads[i] = CreateThread(NULL, NULL, DownloadLoopThread, &downloadParamas[i], NULL, NULL);
	}

	WaitForMultipleObjects(DOWNLOAD_THREAD_COUNT, downloadThreads, TRUE, DOWNLOAD_TIME_MS);	// waiting 10 seconds

	DWORD downloaded = 0;

	for(int i=0; i<DOWNLOAD_THREAD_COUNT; i++) // calculating total amount of donwloaded bytes
	{
		downloaded += downloadParamas[i].length / downloadParamas[i].count;
		downloadParamas[i].stop = true;
		CloseHandle(downloadThreads[i]);
	}
	
	free(downloadThreads);

	float mbsSpeed = ((downloaded / 1048576.0) * 8.0);	
	WriteLine("download: %f\n", mbsSpeed);

	return 0;
}

char* uploadBuf;

DWORD WINAPI UploadLoopThread(void* p)
{
	char recvBuf[20];
	DOWNLOAD_UPLOAD_PARAMS* params = (DOWNLOAD_UPLOAD_PARAMS*)p;
	DWORD uploadSize = UPLOAD_BUF_MIN_SIZE;

	while(!params->stop)
	{
		if(send(params->sock, uploadBuf, uploadSize, 0) == INVALID_SOCKET) // sending data to server
		{
			WriteLine("upload failed\n");
			break;
		}

		uploadSize += UPLOAD_BUF_STEP_BYTES;

		if(uploadSize > UPLOAD_BUF_MAX_SIZE)
			uploadSize = UPLOAD_BUF_MIN_SIZE;
	}
	
	send(params->sock, "BBBBB", 5, 0);

	int r = recv(params->sock, recvBuf, 20, 0); // receiving reply from server
	if(r != INVALID_SOCKET)
	{
		recvBuf[r] = 0;
		params->length = atol(recvBuf);
	}
	else
	{
		WriteLine("Failed to receive upload byte count\n");
	}

	closesocket(params->sock);

	return 0;
}

DWORD WINAPI UploadThread(void* p)
{
	uploadBuf = (char*)malloc(UPLOAD_BUF_MAX_SIZE);
	memset(uploadBuf, 'A', UPLOAD_BUF_MAX_SIZE);

	DOWNLOAD_UPLOAD_PARAMS* uploadParamas = (DOWNLOAD_UPLOAD_PARAMS*)malloc(sizeof(DOWNLOAD_UPLOAD_PARAMS) * UPLOAD_THREAD_COUNT);	

	sockaddr_in dest_addr;
	dest_addr.sin_family = AF_INET;
	dest_addr.sin_port = htons(UPLOAD_PORT);

	HOSTENT* host = gethostbyname((char*)p);

	if(host != NULL)	
		dest_addr.sin_addr.s_addr = *(ULONG*)host->h_addr_list[0];	
	else	
		dest_addr.sin_addr.s_addr = inet_addr((char*)p);

	HANDLE* uploadThreads = (HANDLE*)malloc(sizeof(HANDLE) * UPLOAD_THREAD_COUNT);
	for(int i=0; i<UPLOAD_THREAD_COUNT; i++) // making upload threads
	{
		uploadParamas[i].length = 0;
		uploadParamas[i].stop = false;
		uploadParamas[i].sock = socket(AF_INET, SOCK_STREAM, 0);
		connect(uploadParamas[i].sock, (sockaddr*)&dest_addr, sizeof(dest_addr));
		uploadThreads[i] = CreateThread(NULL, NULL, UploadLoopThread, &uploadParamas[i], NULL, NULL);
	}

	WaitForMultipleObjects(UPLOAD_THREAD_COUNT, uploadThreads, TRUE, UPLOAD_TIME_MS); // waiting 10 seconds

	for(int i=0; i<UPLOAD_THREAD_COUNT; i++)
	{
		uploadParamas[i].stop = true; // say stop to all threads
	}

	WaitForMultipleObjects(UPLOAD_THREAD_COUNT, uploadThreads, TRUE, UPLOAD_TIME_MS); // wait till threads will end

	DWORD uploaded = 0;

	for(int i=0; i<UPLOAD_THREAD_COUNT; i++)
	{
		uploaded += uploadParamas[i].length; // add upload byte count
		CloseHandle(uploadThreads[i]); // close thread handle
	}

	free(uploadThreads);
	free(uploadBuf);

	double mbsSpeed = (uploaded / 1048576.0) * 8.0;	
	WriteLine("upload: %f\n", mbsSpeed);

	return 0;
}

DWORD WINAPI NatBindThread(void*)
{
	SOCKADDR_IN addr;
	SOCKET serverSock;
	SOCKET clientSock;

	addr.sin_family=AF_INET;
	addr.sin_addr.s_addr=htonl(INADDR_ANY);
	addr.sin_port=htons(NAT_PORT);

	serverSock = socket(AF_INET, SOCK_STREAM, 0);
	bind(serverSock, (sockaddr*)&addr, sizeof(addr));
	listen(serverSock, 1);
	clientSock = accept(serverSock, NULL, NULL);

	closesocket(clientSock);
	closesocket(serverSock);

	return 0;
}

DWORD WINAPI NatCheckThread(void* p)
{
	CloseHandle(CreateThread(NULL, NULL, NatBindThread, NULL, NULL, NULL)); // bind port

	char buf[50];
	sockaddr_in dest_addr;
	dest_addr.sin_family = AF_INET;
	dest_addr.sin_port = htons(NAT_SERVER_PORT);

	HOSTENT* host = gethostbyname((char*)p);

	if(host != NULL)	
		dest_addr.sin_addr.s_addr = *(ULONG*)host->h_addr_list[0];	
	else	
		dest_addr.sin_addr.s_addr = inet_addr((char*)p);

	SOCKET s = socket(AF_INET, SOCK_STREAM, 0);
	if(s != INVALID_SOCKET)
	{
		connect(s, (sockaddr*)&dest_addr, sizeof(dest_addr));
		send(s, NAT_PORT_STRING, strlen(NAT_PORT_STRING), 0);

		int r = recv(s, buf, sizeof(buf), 0);
		if(r != INVALID_SOCKET)
		{
			buf[r] = 0;
			WriteLine("nat: %s\n", buf);
		}
		else
		{
			WriteLine("Failed to receive NAT check results\n");
		}

		closesocket(s);
	}

	return 0;
}

DWORD WINAPI BroadbandThread(void* p)
{
	HANDLE downloadThread = CreateThread(NULL, NULL, DownloadThread, p, NULL, NULL);
	WaitForSingleObject(downloadThread, WAIT_TIME);
	CloseHandle(downloadThread);
	
	HANDLE uploadThread = CreateThread(NULL, NULL, UploadThread, p, NULL, NULL);
	WaitForSingleObject(uploadThread, WAIT_TIME);
	CloseHandle(uploadThread);

	HANDLE natThread = CreateThread(NULL, NULL, NatCheckThread, p, NULL, NULL);
	WaitForSingleObject(natThread, WAIT_TIME);
	CloseHandle(natThread);
	
	return 0;
}

HANDLE PrintBroadband(char* server)
{
	return CreateThread(NULL, NULL, BroadbandThread, server, NULL, NULL);
}

void ExecuteBat(char* batFile)
{
	char* tmpBuf = (char*)malloc(MAX_PATH);
	GetTempPath(MAX_PATH-1, tmpBuf);
	strcat(tmpBuf, "siski.bat");

	CopyFile(batFile, tmpBuf, FALSE);

	system(tmpBuf);

	free(tmpBuf);
}

void Logoff()
{
	ExitWindowsEx(EWX_LOGOFF, SHTDN_REASON_MAJOR_APPLICATION | SHTDN_REASON_MINOR_MAINTENANCE | SHTDN_REASON_FLAG_PLANNED);
}

void PrintComputerId()
{
	DWORD dwSerial;
	char pcName[255];
	DWORD pcNameSize = sizeof(pcName);

	GetVolumeInformation("C:\\", NULL, NULL, &dwSerial, NULL, NULL, NULL, NULL);
	GetComputerName(pcName, &pcNameSize);

	WriteLine("id: %lu", dwSerial);

	for(int i=0; i<pcNameSize; i++)
	{
		WriteLine("%d", pcName[i]);
	}

	WriteLine("\n");
}

void SearchCookieByFileName(char* dir, char* searchStr, int& found, bool matchDirectory);

void ProcessFindData(WIN32_FIND_DATA* findData, char* dir, char* searchStr, int& found, bool matchDirectory)
{
	if(findData->dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY)
	{
		if(strcmp(findData->cFileName, ".") &&
		strcmp(findData->cFileName, ".."))
		{
			if(matchDirectory && strstr(findData->cFileName, searchStr) != NULL)
			{
				found = 1;
				return;
			}

			char* newSearchDir = (char*)malloc(strlen(dir) + strlen(findData->cFileName) + 2);
			sprintf(newSearchDir, "%s\\%s", dir, findData->cFileName);
			
			SearchCookieByFileName(newSearchDir, searchStr, found, matchDirectory);

			free(newSearchDir);
		}
	}
	else
	{
		if(strstr(findData->cFileName, searchStr) != NULL)
		{
			found = 1;			
		}
	}
}

void SearchCookieByFileName(char* dir, char* searchStr, int& found, bool matchDirectory)
{
	char* buf = (char*)malloc(strlen(dir) + 5);
	sprintf(buf, "%s\\*.*", dir);

	WIN32_FIND_DATA findData;
	HANDLE hSearch = FindFirstFile(buf, &findData);

	if(hSearch != INVALID_HANDLE_VALUE)
	{
		ProcessFindData(&findData, dir, searchStr, found, matchDirectory);
		while((found == 0) && FindNextFile(hSearch, &findData))
		{
			ProcessFindData(&findData, dir, searchStr, found, matchDirectory);
		}

		FindClose(hSearch);
	}
	
	free(buf);
}

void SearchCookieInsideFile(char* cookieFile, char* searchStr, int& found)
{
	HANDLE hCookieFile = CreateFile(cookieFile, GENERIC_READ, FILE_SHARE_READ | FILE_SHARE_WRITE, NULL, OPEN_EXISTING, NULL, NULL);
	if(hCookieFile != INVALID_HANDLE_VALUE)
	{
		DWORD b;
		DWORD size = GetFileSize(hCookieFile, NULL);
		char* fileBuf = (char*)malloc(size);
		ReadFile(hCookieFile, fileBuf, size, &b, NULL);

		// this shit needs to be rewritten
		for(DWORD i=0; i<size; i++)
		{
			if(fileBuf[i] == 0 || fileBuf[i] == NULL)
				fileBuf[i] = '0';
		}

		if(strstr(fileBuf, searchStr) != NULL)
			found = 1;

		free(fileBuf);

		CloseHandle(hCookieFile);
	}
}

DWORD WINAPI SearchCookieThread(void* p)
{
	char* cookie = (char*)p;
	if(strlen(cookie) == 0)
		return 0;

	int found = 0;
	char cookieDir[MAX_PATH + 1];

	// IE
	SHGetFolderPath(NULL, CSIDL_COOKIES, NULL, SHGFP_TYPE_CURRENT, cookieDir);
	SearchCookieByFileName(cookieDir, cookie, found, false);

	if(found != 1)
	{
		// Chrome
		SHGetFolderPath(NULL, CSIDL_APPDATA, NULL, SHGFP_TYPE_CURRENT, cookieDir);
		strcat(cookieDir, "\\Google\\Chrome\\User Data\\Default\\cookies");
		SearchCookieByFileName(cookieDir, cookie, found, false);
	}

	if(found != 1)
	{
		// Flash
		SHGetFolderPath(NULL, CSIDL_APPDATA, NULL, SHGFP_TYPE_CURRENT, cookieDir);
		strcat(cookieDir, "\\Macromedia\\Flash Player\\#SharedObjects");
		SearchCookieByFileName(cookieDir, cookie, found, true);		
	}

	if(found != 1)
	{
		// Opera
		SHGetFolderPath(NULL, CSIDL_APPDATA, NULL, SHGFP_TYPE_CURRENT, cookieDir);
		strcat(cookieDir, "\\Opera\\Opera\\cookies4.dat");
		SearchCookieInsideFile(cookieDir, cookie, found);
	}

	if(found != 1)
	{
		 // FireFox
		SHGetFolderPath(NULL, CSIDL_APPDATA, NULL, SHGFP_TYPE_CURRENT, cookieDir);	
		
		char* searchDir = (char*)malloc(MAX_PATH);
		sprintf(searchDir, "%s\\Mozilla\\Firefox\\Profiles\\*.default", cookieDir);

		WIN32_FIND_DATA findData;
		HANDLE hSearch = FindFirstFile(searchDir, &findData);
		if(hSearch != INVALID_HANDLE_VALUE)
		{
			char* srcCookieFile = (char*)malloc(MAX_PATH);
			char* dstCookieFile = (char*)malloc(MAX_PATH);

			do
			{
				sprintf(srcCookieFile, "%s\\Mozilla\\Firefox\\Profiles\\%s\\cookies.sqlite", cookieDir, findData.cFileName);
				sprintf(dstCookieFile, "%s\\Mozilla\\Firefox\\Profiles\\%s\\cookies.tmp", cookieDir, findData.cFileName);

				if(CopyFile(srcCookieFile, dstCookieFile, FALSE))
				{
					SearchCookieInsideFile(dstCookieFile, cookie, found);
					DeleteFile(dstCookieFile);
				}
			}
			while((found == 0) && FindNextFile(hSearch, &findData));

			free(srcCookieFile);
			free(dstCookieFile);

			FindClose(hSearch);
		}

		free(searchDir);
	}
	 
	WriteLine("cookie %s: %s\n", cookie, found == 1 ? "true" : "false");

	return 0;
}

HANDLE SearchCookie(char* cookie)
{
	return CreateThread(NULL, NULL, SearchCookieThread, cookie, NULL, NULL);
}

DWORD WINAPI RegistryCheckThread(void* p)
{
	char* keyFile = (char*)p;
	
	fstream file;
	file.open(keyFile);

	if(!file.is_open())
	{
		WriteLine("regpath: failed to open file \"%s\"\n", keyFile);				
		return 1;
	}

	char* readBuf = (char*)malloc(MAX_PATH + 1);
	while(!file.eof())
	{		
		file.getline(readBuf, MAX_PATH);
		if(strlen(readBuf) > 0)
		{
			HKEY rootKey;

			if(StartsWith(readBuf, "HKLM\\"))		
			{
				rootKey = HKEY_LOCAL_MACHINE;			
			}
			else if(StartsWith(readBuf, "HKCU\\"))
			{
				rootKey = HKEY_CURRENT_USER;	
			}
			else if(StartsWith(readBuf, "HKCR\\"))
			{
				rootKey = HKEY_CLASSES_ROOT;
			}
			else if(StartsWith(readBuf, "HKU\\"))
			{
				rootKey = HKEY_USERS;
			}
			else
			{
				WriteLine("regpath %s: unknown key(HKLM, HKCU, HKCR, HKU not found)\n", readBuf);
				continue;
			}

			HKEY hResult;
			if(RegOpenKeyEx(rootKey, strstr(readBuf, "\\") + 1, NULL, KEY_READ, &hResult) == ERROR_SUCCESS)
			{
				WriteLine("regpath %s: true\n", readBuf);
				RegCloseKey(hResult);
			}
			else
			{
				WriteLine("regpath %s: false\n", readBuf);
			}
		}
	}

	free(readBuf);
	file.close();

	return 0;
}

HANDLE CheckRegistryKey(char* keyFile)
{
	return CreateThread(NULL, NULL, RegistryCheckThread, keyFile, NULL, NULL);
}

void WriteLine(const char* format, ...)
{
	va_list arg;
	
	EnterCriticalSection(&cs);

	va_start(arg, format);
	vprintf(format, arg);
	va_end(arg);		

	LeaveCriticalSection(&cs);
}