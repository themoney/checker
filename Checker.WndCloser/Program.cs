﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.InteropServices;
using System.Diagnostics;
using System.Configuration;
using System.ComponentModel;
using System.Threading;

namespace Checker.WndCloser
{
    class Program
    {
        static void Main(string[] args)
        {
            BackgroundWorker bw = new BackgroundWorker();
            bw.DoWork += new DoWorkEventHandler(bw_DoWork);
            bw.WorkerSupportsCancellation = true;
            bw.RunWorkerAsync();

            Console.WriteLine("Window closer started\nPress any key to exit...");
            Console.ReadKey();

            bw.CancelAsync();
            bw.DoWork -= bw_DoWork;
        }

        static void bw_DoWork(object sender, DoWorkEventArgs e)
        {
            while (true)
            {
                CloseStupidWindows();
                Thread.Sleep(5000);
            }
        }

        static void CloseStupidWindows()
        {
            List<IntPtr> windowHandles = new List<IntPtr>();

            Process[] checkerProcesses = Process.GetProcessesByName(ConfigurationManager.AppSettings["ProcessName"]);
            foreach (Process checker in checkerProcesses)
            {
                foreach (ProcessThread pt in checker.Threads)
                {
                    EnumThreadWindows(pt.Id,
                        (hwnd, lParam) =>
                        {
                            windowHandles.Add(hwnd);

                            EnumChildWindows(hwnd,
                                (h, l) =>
                                {
                                    windowHandles.Add(h);
                                    return true;
                                },
                                IntPtr.Zero);

                            return true;
                        },
                        IntPtr.Zero);
                }
            }

            foreach (IntPtr wndHandle in windowHandles)
            {
                int textLength = GetWindowTextLength(wndHandle);
                if (textLength > 0)
                {
                    StringBuilder sb = new StringBuilder(textLength + 1);
                    GetWindowText(wndHandle, sb, sb.Capacity);

                    string windowText = sb.ToString().ToUpper();
                    string buttonsString = ConfigurationManager.AppSettings["ButtonsToClick"].ToUpper();
                    string[] buttons = buttonsString.Split(',');

                    if (buttons.Contains(windowText))
                    {
                        Console.WriteLine("Pressing '{0}' button with handle = {1}", windowText, wndHandle);
                        PostMessage(wndHandle, 0x0201, 0x0001, 0x00050005);
                        PostMessage(wndHandle, 0x0202, 0, 0x00050005);
                    }
                }
            }
        }

        [DllImport("user32.dll", SetLastError = true)]
        public static extern IntPtr PostMessage(IntPtr hWnd, uint uMsg, int wParam, int lParam);

        public delegate bool Win32Callback(IntPtr hWnd, IntPtr lParam);

        [DllImport("user32.dll")]
        public static extern bool EnumThreadWindows(int dwThreadId, Win32Callback lpfn, IntPtr lParam);

        [DllImport("user32.dll", CharSet = CharSet.Auto, SetLastError = true)]
        public static extern int GetWindowText(IntPtr hWnd, StringBuilder lpString, int nMaxCount);

        [DllImport("user32.Dll")]
        [return: MarshalAs(UnmanagedType.Bool)]
        public static extern bool EnumChildWindows(IntPtr parentHandle, Win32Callback callback, IntPtr lParam);

        [DllImport("user32.dll", SetLastError = true, CharSet = CharSet.Auto)]
        public static extern int GetWindowTextLength(IntPtr hWnd);
    }
}
